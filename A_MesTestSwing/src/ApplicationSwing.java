import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.border.BevelBorder;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JProgressBar;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.border.LineBorder;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultMutableTreeNode;
/**
 * classe Wb tester
 * 
 * @author Mina Ghobrial
 *
 */

public class ApplicationSwing extends JFrame {

	private JPanel contentPane;
	private final ButtonGroup bgrCouleurFond = new ButtonGroup();
	private JCheckBox chckbxFondClair;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ApplicationSwing frame = new ApplicationSwing();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ApplicationSwing() {
		//Couleur = new Couleur();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 788, 589);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnMenuEnregistrer = new JMenu("Menu");
		menuBar.add(mnMenuEnregistrer);
		
		JMenuItem mnuItemEnregistrerSous = new JMenuItem("Enregistrer Sous");
		mnMenuEnregistrer.add(mnuItemEnregistrerSous);
		
		JMenu mnuEnregistrer = new JMenu("Edit");
		menuBar.add(mnuEnregistrer);
		
		JMenuItem mnuItemEnregistrer = new JMenuItem("Enregistrer");
		mnuEnregistrer.add(mnuItemEnregistrer);
		
		JMenu mnuQuitter = new JMenu("window");
		menuBar.add(mnuQuitter);
		
		JMenuItem mnuItemQuitter = new JMenuItem("Quitter");
		mnuItemQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/**/
				System.exit(0);
				
				/**/
			}
		});
		mnuQuitter.add(mnuItemQuitter);
		
		JMenu mnuInfo = new JMenu("About");
		menuBar.add(mnuInfo);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("InfoText");
		mnuInfo.add(mntmNewMenuItem);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		chckbxFondClair = new JCheckBox("Couleur de Fond en clair");
		chckbxFondClair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/**/
				
				
				
				/**/
			}
		});
		chckbxFondClair.setBounds(23, 34, 149, 47);
		contentPane.add(chckbxFondClair);
		
		JButton btnAppliquerCouleur = new JButton("appliquer");
		btnAppliquerCouleur.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(chckbxFondClair.isSelected()) {
					
				}
			}
		});
		btnAppliquerCouleur.setBounds(47, 114, 89, 23);
		contentPane.add(btnAppliquerCouleur);
		
		JPanel jpnlFond = new JPanel();
		jpnlFond.setBackground(Color.CYAN);
		jpnlFond.setForeground(Color.RED);
		jpnlFond.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)), "Fond", TitledBorder.LEADING, TitledBorder.TOP, null, Color.ORANGE));
		jpnlFond.setBounds(593, 34, 93, 144);
		contentPane.add(jpnlFond);
		jpnlFond.setLayout(null);
		
		JRadioButton rdbtnFondBleu = new JRadioButton("fond bleu");
		rdbtnFondBleu.setBounds(6, 16, 81, 23);
		jpnlFond.add(rdbtnFondBleu);
		rdbtnFondBleu.setForeground(Color.CYAN);
		rdbtnFondBleu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/**/
				
				contentPane.setBackground(Color.cyan);
				
				/**/
			}
		});
		rdbtnFondBleu.setSelected(true);
		bgrCouleurFond.add(rdbtnFondBleu);
		
		JRadioButton rdbtnFondVert = new JRadioButton("fond vert");
		rdbtnFondVert.setBounds(6, 64, 81, 23);
		jpnlFond.add(rdbtnFondVert);
		rdbtnFondVert.setForeground(Color.GREEN);
		rdbtnFondVert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				contentPane.setBackground(Color.green);
			}
		});
		bgrCouleurFond.add(rdbtnFondVert);
		
		JRadioButton rdbtnFondRouge = new JRadioButton("fond rouge");
		rdbtnFondRouge.setBounds(6, 115, 81, 23);
		jpnlFond.add(rdbtnFondRouge);
		rdbtnFondRouge.setForeground(Color.RED);
		rdbtnFondRouge.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.red);
			}
		});
		bgrCouleurFond.add(rdbtnFondRouge);
		
		JComboBox cboFruit = new JComboBox();
		cboFruit.setModel(new DefaultComboBoxModel(new String[] {"pomme", "banane", "poire"}));
		cboFruit.setBounds(268, 84, 129, 23);
		contentPane.add(cboFruit);
		
		JSpinner snprAge = new JSpinner();
		snprAge.setModel(new SpinnerNumberModel(10, 0, 20, 5));
		snprAge.setBounds(466, 94, 67, 62);
		contentPane.add(snprAge);
		
		JTabbedPane tbpDonnerInfo = new JTabbedPane(JTabbedPane.TOP);
		tbpDonnerInfo.setBounds(191, 331, 242, 172);
		contentPane.add(tbpDonnerInfo);
		
		JPanel panelEtape1 = new JPanel();
		tbpDonnerInfo.addTab("Etape1", null, panelEtape1, null);
		panelEtape1.setLayout(null);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.setBounds(38, 21, 89, 23);
		panelEtape1.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("New button");
		btnNewButton_1.setBounds(38, 67, 89, 23);
		panelEtape1.add(btnNewButton_1);
		
		JPanel panelEtape2 = new JPanel();
		tbpDonnerInfo.addTab("Etape2", null, panelEtape2, null);
		panelEtape2.setLayout(null);
		
		JButton btnNewButton_2 = new JButton("New button");
		btnNewButton_2.setBounds(38, 22, 89, 23);
		panelEtape2.add(btnNewButton_2);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("New check box");
		chckbxNewCheckBox.setBounds(71, 68, 22, 23);
		panelEtape2.add(chckbxNewCheckBox);
		
		JPanel panelEtape3 = new JPanel();
		tbpDonnerInfo.addTab("Etape3", null, panelEtape3, null);
		panelEtape3.setLayout(null);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(35, 11, 95, 31);
		panelEtape3.add(passwordField);
		
		JButton btnNewButton_3 = new JButton("New button");
		btnNewButton_3.setBounds(38, 65, 89, 23);
		panelEtape3.add(btnNewButton_3);
		
		JSeparator separator = new JSeparator();
		separator.setForeground(Color.RED);
		separator.setBounds(-25, 189, 797, 51);
		contentPane.add(separator);
		
		JSlider sliderAge = new JSlider();
		sliderAge.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				/**/
				
				System.out.println("salut");
				
				/**/
			}
		});
		sliderAge.setSnapToTicks(true);
		sliderAge.setMinorTickSpacing(5);
		sliderAge.setMajorTickSpacing(10);
		sliderAge.setPaintTicks(true);
		sliderAge.setPaintLabels(true);
		sliderAge.setValue(20);
		sliderAge.setBounds(191, 251, 242, 69);
		contentPane.add(sliderAge);
		
		JProgressBar pbarVolume = new JProgressBar();
		pbarVolume.setStringPainted(true);
		pbarVolume.setValue(20);
		pbarVolume.setForeground(Color.RED);
		pbarVolume.setBackground(Color.CYAN);
		pbarVolume.setBounds(466, 274, 283, 39);
		contentPane.add(pbarVolume);
		
		JScrollPane scrlPaneJour = new JScrollPane();
		scrlPaneJour.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrlPaneJour.setViewportBorder(new LineBorder(new Color(0, 0, 0)));
		scrlPaneJour.setBounds(35, 274, 93, 82);
		contentPane.add(scrlPaneJour);
		
		JList listSemaine = new JList();
		scrlPaneJour.setViewportView(listSemaine);
		listSemaine.setModel(new AbstractListModel() {
			String[] values = new String[] {"lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		
		JTree treeFruit = new JTree();
		treeFruit.setModel(new DefaultTreeModel(
			new DefaultMutableTreeNode("JTree") {
				{
					DefaultMutableTreeNode node_1;
					node_1 = new DefaultMutableTreeNode("Fruit");
						node_1.add(new DefaultMutableTreeNode("Pomme"));
						node_1.add(new DefaultMutableTreeNode("Banane"));
					add(node_1);
					node_1 = new DefaultMutableTreeNode("sports");
						node_1.add(new DefaultMutableTreeNode("basketball"));
						node_1.add(new DefaultMutableTreeNode("soccer"));
						node_1.add(new DefaultMutableTreeNode("football"));
						node_1.add(new DefaultMutableTreeNode("hockey"));
					add(node_1);
					node_1 = new DefaultMutableTreeNode("food");
						node_1.add(new DefaultMutableTreeNode("hot dogs"));
						node_1.add(new DefaultMutableTreeNode("pizza"));
						node_1.add(new DefaultMutableTreeNode("ravioli"));
						node_1.add(new DefaultMutableTreeNode("bananas"));
					add(node_1);
				}
			}
		));
		treeFruit.setBounds(491, 354, 149, 132);
		contentPane.add(treeFruit);
		
		JButton btnNewButton_4 = new JButton("afficher PAlette");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
			}
		});
		btnNewButton_4.setBounds(22, 408, 114, 23);
		contentPane.add(btnNewButton_4);
		
		
		
		
	}
}
